from api.model_simple import get_schema


def print_every_infos(schema_name):
    """

        Will show every informations from a schema and his relative objects/actions/roles...
        There is no use of this but only to visualise a whole schema.

        :param schema_name: The schema name that you want to see all relations

    """
    try:
        tmp_schema = get_schema(schema_name)
        print("#################")
        print("# COMPLETE ROLE #")
        print("#################")
        # SCHEMA
        print("### SCHEMA ###")
        print("# NAME : " + schema_name)
        print("# RELATED OBJECTS :", tmp_schema.LinkedObjects)
        print("# RELATED ACTIONS :", tmp_schema.LinkedActions)
        print("# RELATED MetaATTR: ", tmp_schema.LinkedMetaAttr)
        # RELATED OBJECTS
        print("### OBJECTS ###")
        for elem in tmp_schema.LinkedObjects:
            print("# OBJ NAME : " + elem.name)
            print("# RELATED ATTR :", elem.LinkedAttributes)
            print("# RELATED PERMISSIONS :", elem.LinkedPerm)
            for perm_elem in elem.LinkedPerm:
                print("# RELATED ROLE: ", perm_elem.LinkedRole)
                for role_elem in perm_elem.LinkedRole:
                    print("# RELATED ROLES USERS :", role_elem.LinkedUsers)
        print("### ACTIONS ###")
        for elem in tmp_schema.LinkedActions:
            print("# ACTION NAME : " + elem.action)
            print("# RELATED PERMISSIONS :", elem.LinkedPerm)
            for perm_elem in elem.LinkedPerm:
                print("# RELATED ROLE: ", perm_elem.LinkedRole)
                for role_elem in perm_elem.LinkedRole:
                    print("# RELATED ROLES USERS :", role_elem.LinkedUsers)
        print("### METAATTR ###")
        for elem in tmp_schema.LinkedMetaAttr:
            print("# MetaATTR NAME : " + elem.name)
            print("# MetaATTR TYPE :", elem.type_str)
            print("# RELATED ATTR  :", elem.LinkedAttributes)
        print("### END ###")
    except Exception as e:
        print(e)
