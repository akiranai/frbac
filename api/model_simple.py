from api import string_constants as CONST
from api.tools import salt_gen, sbkdf2_super
from model import Frbacrole, Frbacaction, Frbacuser, Frbacschema, Frbacmetaattribute, Frbacattribute, Frbacobject, \
    Frbacpermission


def add_role(name: str):
    """

        Try to add a perm and return it, if already exist will except

        :param name: The name of the role

    """
    try:
        if not name or name is None: return None
        return Frbacrole(name=name)
    except Exception as e:
        print(e)
        return None


def add_user(name: str, role: str = None):
    """

        Try to add a user and return it, if already exist will except

        :param name: The name of the user to add
        :param role: The name of the role to link to (OPTIONAL)

    """
    try:
        if not name or name is None: return None
        if role is None: return Frbacuser(username=name)
        usr = Frbacuser(username=name)
        (get_role(role)).update_user_relation(name)
        return usr
    except Exception as e:
        print(e)
        return None


def set_user_role(name: str, role: str):
    """

    Define a related perm for an user

    :param name: The name of the user
    :param role: The name of the related perm

    """
    try:
        if get_role(role) is None or get_user(name) is None: return None
        get_role(role).update_user_relation(name)
    except Exception as e:
        print(e)
        return None


def set_perm_role(role: str, perm: str):
    """

    Allow to add a permission to a role

    :param role: The name of the role
    :param perm: The name of the permission to add to a role

    """
    try:
        if get_role(role) is None or get_permission(perm) is None: return None
        get_role(role).update_perm_relation(perm)
    except Exception as e:
        print(e)
        return None


def add_action(action: str, schema, display_name: str):
    """

        Add a new action to the table and return it

        :param action: The name of the action to add
        :param display_name: The display name of the action

    """
    try:

        x = Frbacaction(action=action, frbacschema=Frbacschema.byName(schema), display_name=display_name)
        print(CONST.ADD_TO_DATABASE.format(action))
        return x
    except Exception as e:
        print(e)
        return None


def add_schema(name: str):
    """

        Try to add a schema and return it, if already exist will except

        :param name: The name of the schema to add

    """
    try:
        print(CONST.ADD_TO_DATABASE.format(name))
        return Frbacschema(name=name)
    except Exception as e:
        print(e)
        pass


def add_metaattr(name: str, display_name: str, type_str: str, schema):
    """

        Try to add a meta attribute and return it, if already exist will except

    """
    try:
        x = Frbacmetaattribute(name=name, display_name=display_name, type_str=type_str,
                               frbacschema=Frbacschema.byName(schema))
        print(CONST.ADD_TO_DATABASE.format(name))
        return x
    except Exception as e:
        print(e)
        return None


def add_attr(value: str, metaattr, obj):
    """

        Try to add an attribute and return it, if already exist will except

    """
    try:
        x = Frbacattribute(value=value, frbacmetaattribute=Frbacmetaattribute.byName(metaattr),
                           frbacobject=Frbacobject.byName(obj))
        print(CONST.ADD_TO_DATABASE.format(value))
        return x
    except Exception as e:
        print(e)
        return None


def add_object(obj, schema):
    """

        Try to create a relation between object and a FK schema

    """
    try:

        x = Frbacobject(name=obj, frbacschema=Frbacschema.byName(schema))
        print(CONST.ADD_TO_DATABASE.format(schema))
        return x
    except Exception as e:
        print(e)
        pass


def remove_user(name: str):
    """

        Try to remove a user, if does not exist, will except

    """
    try:
        Frbacuser.deleteBy(username=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_role(name: str):
    """

        Try to remove a role, if does not exist, will except

    """
    try:
        Frbacrole.deleteBy(name=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_permission(name: str):
    """

        Try to remove a perm if does not exist, will except

    """
    try:
        Frbacpermission.deleteBy(name=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_action(name: str):
    """

        Remove an action of the table ACTION

    """
    try:
        Frbacaction.deleteBy(action=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_object(name: str):
    """
        Remove a specific object

        :param name: name of the object to remove

    """
    try:
        Frbacobject.deleteBy(name=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_schema(name):
    """

        Remove a specific schema from the table SCHEMA

    """
    try:
        Frbacschema.deleteBy(name=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_metaattr(name: str):
    """

        Remove a specific metaattr from the table METAATTR

    """
    try:
        Frbacmetaattribute.deleteBy(name=name)
        print(CONST.REMOVE_TO_DATABASE.format(name))
    except Exception as e:
        print(e)
        pass


def remove_attr(value):
    """

        Remove a specific attribute from the table ATTRIBUTE

    """
    try:
        Frbacattribute.deleteBy(value=value)
        print(CONST.REMOVE_TO_DATABASE.format(value))
    except Exception as e:
        print(e)
        pass


def get_action(name):
    """

        Get a specific action

    """
    try:
        return Frbacaction.byAction(name)
    except Exception as e:
        print(e)
        return None


def get_user(name):
    """

        Get a specific USER

    """
    try:
        return Frbacuser.byUsername(name)
    except Exception as e:
        print(e)
        return None


def get_role(name):
    """

        Get a specific ROLE

    """
    try:
        return Frbacrole.byName(name)
    except Exception as e:
        print(e)
        return None


def get_schema(name):
    """

        Get a specific schema from the table SCHEMA

    """
    try:
        return Frbacschema.byName(name)
    except Exception as e:
        print(e)
        return None


def get_metaattr(name):
    """

        Get a specific meta attribute from the table Metaattr

    """
    try:
        return Frbacmetaattribute.byName(name)
    except Exception as e:
        print(e)
        return None


def get_attr(value):
    """

        Get a specific attribute from the table ATTRIBUTE

    """
    try:
        return Frbacattribute.byValue(value)
    except Exception as e:
        print(e)
        return None


def get_obj(name):
    """

        Get a specific object from the table OBJECT

    """
    try:
        return Frbacobject.byName(name)
    except Exception as e:
        print(e)
        return None


def add_permission(name: str):
    """
    Add a permission

    :param name: the name of the permission to add


    """
    try:
        if not name or name is None: return None
        return Frbacpermission(name=name, frbacaction=None, frbacobject=None)
    except Exception as e:
        print(e)
        return None


def get_permission(name: str):
    """

    Get a specific permission

    :param name: The name of the permission you want to have
    :return: The permission, if not exist, None

    """
    try:
        return Frbacpermission.byName(name)
    except Exception as e:
        print(e)
        return None


def perm_relations(name: str, act: str, obj: str):
    """

    Allow to set relations of a permission, it can be action or obj

    :param name: The name of the permission
    :param act: The display name of the action to link to
    :param obj: The object to relate to
    :return: boolean

    """
    try:
        tmp_perm = get_permission(name)
        tmp_perm.frbacaction = Frbacaction.byAction(act)
        tmp_perm.frbacobject = Frbacobject.byName(obj)
        return True
    except Exception as e:
        print(e)
        return False


def set_user_key_salt(username, key, salt):
    """

    Set the key and the salt of an user

    """
    try:
        user = get_user(username)
        user.key = key
        user.salt = salt
        return True
    except Exception as e:
        print(e)
        return False


def set_user_pwd(username, pwd):
    """

    Set the password of an user

    :param username:
    :param pwd:

    """
    salt = salt_gen()
    set_user_key_salt(username, salt=salt, key=sbkdf2_super(pwd=pwd, salt=salt))


def get_user_key(username):
    """

    Get the key of an user for the password

    :param username:
    :return: The user's key

    """
    return get_user(username).key


def get_user_salt(username):
    """
    Get the salt of an user for the password

    :param username:
    :return: The user's salt
    """
    return get_user(username).salt

