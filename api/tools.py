# List of useful methods
import hashlib
import os


def random_string(size):
    """
        Generate random strings
        [!] do not use it for crypto - not secure
    """
    import string
    import random
    return ''.join(random.choices(string.ascii_lowercase +
                                  string.digits, k=size))


def sbkdf2_super(salt, pwd):
    """

    Allow to hash password in sha256, see https://docs.python.org/3/library/hashlib.html

    :param salt:
    :param pwd:
    :return: the password hashed

    """
    return hashlib.pbkdf2_hmac('sha256', pwd.encode('utf-8'), salt, 100000)


def salt_gen():
    """

    Generate a random salt

    :return: the generated salt

    """
    return os.urandom(32)
