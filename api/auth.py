from api import model_simple as MS
from api.model_simple import get_user, get_user_key, get_user_salt
from api.tools import sbkdf2_super
from model import Frbacaction, Frbacuser
from api.model_tools import any_linked


def request_auth_join(usr, display_name, obj, pwd=None):
    """

        Check if a user have permissions to act on an action

        :param usr: The utilisateur to authentifie
        :param display_name: The display name of an action, not the alternate ID name of the action itself !
        :param obj: The name of the object to match
        :param pwd: The password if there is one of the user

    """
    try:
        y = get_user(usr)
        for x in Frbacaction.selectBy(display_name=display_name):
            if x.LinkedPerm == MS.get_obj(obj).LinkedPerm and y.LinkedRole == x.LinkedPerm[0].LinkedRole:
                if y.salt is not None and y.key is not None:
                    if get_user_key(usr) == sbkdf2_super(pwd=pwd, salt=get_user_salt(usr)):
                        return True
                    return False
                return True
        return False
    except Exception as e:
        print(e)
        return False
