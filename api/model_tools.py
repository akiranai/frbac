# Methods relative to model.py
import inspect


def table_create():
    """

        Create all tables from model.py

    """
    import model
    for cls in inspect.getmembers(model, inspect.isclass):
        if cls[1].__module__ == 'model': cls[1].createTable()


def table_drop():
    """

        Drop all tables from model.py

    """
    import model
    for cls in inspect.getmembers(model, inspect.isclass):
        if cls[1].__module__ == 'model':
            cls[1].dropTable()


def any_linked(related_obj, col_name, compare_string):
    """

        check if a specific string exist in a row in a table

    """

    return any(elem for elem in related_obj if getattr(elem, col_name) == compare_string)
