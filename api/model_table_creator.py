﻿from sqlobject import sqlhub

from api.model_simple import add_schema, get_action, add_action, get_metaattr, add_metaattr, get_role, add_role, \
    get_attr, add_attr, add_object, add_user, get_schema, get_obj, get_user, perm_relations, get_permission, \
    add_permission


def create_schema( name: str, action_display_name: list[str] = None, metaattr: list[tuple[str, str]] = None ) -> bool:
    """

    Allow to create a schema with his related actions and meta attribute

    :param name: The name of the schema
    :param action_display_name: optional, list of action related to the schema
    :param metaattr: optional, list of meta attribute related to the schema
    :return: boolean

    """
    try:
        trans = sqlhub.processConnection.transaction()
        if metaattr == [None]: return False
        if get_schema(name) is None: add_schema(name)
        # for each action_display_name create name and link to schema
        if action_display_name is not None:
            if not action_display_name or len(action_display_name) != len(set(action_display_name)): return False
            for x in action_display_name:
                y = create_action(x, name)
                if not y: return False
        if metaattr is not None:
            for x in metaattr:
                if len(x) != 2: return False
                y = create_metaattr(x[0], x[1], name)
                if not y: return False
        trans.commit()
        return True
    except Exception as e:
        print(e)
        return False


def create_action( display_name: str, schema: str ) -> bool:
    """

    Allow to create an action with the related schema
    The name is generated with the display_name and the schema name

    :param display_name: the display name of the action
    :param schema: the name of the schema related to
    :return: boolean

    """
    try:
        var_a = schema + '_' + display_name
        if get_action(var_a) is None:
            trans = sqlhub.processConnection.transaction()
            add_action(var_a, schema, display_name)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_metaattr( display_name: str, type_str: str, schema: str ) -> bool:
    """

    allow to create a meta attribute with related schema.
    The name is generated with the display_name and the schema name

    :param display_name: The display name of the meta attribute
    :param type_str: the type of the mete attribute (str, float, int..)
    :param schema: the name of the related schema
    :return: boolean

    """
    try:
        if display_name is None or type_str is None or schema is None or get_schema(schema) is None: return False
        if not display_name or not type_str or not schema: return False
        var_a = schema + "_" + display_name
        if get_metaattr(var_a) is None:
            trans = sqlhub.processConnection.transaction()
            y = add_metaattr(var_a, display_name, type_str, schema)
            if y is None: return False
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_object( obj: str, schema: str ) -> bool:
    """

    Allow to create an object with the related schema

    :param obj: The name of the object
    :param schema: the name of the related schema
    :return: boolean

    """
    try:
        if obj is None or schema is None or get_schema(schema) is None: return False
        if not obj or not schema: return False
        if get_obj(obj) is None:
            trans = sqlhub.processConnection.transaction()
            add_object(obj, schema)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_attribute( value: str, obj: str, meta: str ) -> bool:
    """

    Allow to create an attribute with the related meta attribute and object

    :param obj: The object related to
    :param meta: The meta attribute relatd to
    :param value: the value of the attribute
    :return: boolean

    """
    try:
        if obj is None or meta is None or value is None: return False
        if not obj or not meta or not value: return False
        if get_obj(obj) is None or get_metaattr(meta) is None: return False
        if get_attr(value) is None:
            trans = sqlhub.processConnection.transaction()
            add_attr(value, meta, obj)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_role( name: str ):
    """

    Allow to create a perm

    :param name: The name of the perm
    :return: boolean

    """
    try:
        if not name or name is None: return False
        if get_role(name) is None:
            trans = sqlhub.processConnection.transaction()
            add_role(name)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_permission( name: str ):
    """

    Allow to create a perm

    :param name: The name of the perm
    :return: boolean

    """
    try:
        if not name or name is None: return False
        if get_permission(name) is None:
            trans = sqlhub.processConnection.transaction()
            add_permission(name)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_user( name: str, role: str = None ):
    """

    Allow to create an user and to link to a related perm if specified

    :param name: the name of the user
    :param role: the name of the perm
    :return: boolean

    """
    try:
        if not name or name is None: return False
        if get_user(name) is None:
            trans = sqlhub.processConnection.transaction()
            if role and get_role(role) is not None:
                add_user(name, role)
            elif role and get_role(role) is None:
                return False
            else:
                add_user(name)
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return e


def set_user_role( name: str, role: str ):
    """

    Allow to define the related perm of an user

    :param name: The name of the user
    :param role: The name of the perm
    :return: boolean

    """
    try:
        if not name or name is None or not role or role is None: return False
        if get_role(role) is None or get_user(name) is None: return False
        trans = sqlhub.processConnection.transaction()
        x = get_role(role)
        x.update_user_relation(name)
        trans.commit()
        return True
    except Exception as e:
        print(e)
        return False


def set_perm_role( perm: str, role: str ):
    """

    Allow to define the related perm of an user

    :param perm: The name of the perm
    :param role: The name of the perm
    :return: boolean

    """
    try:
        if not perm or perm is None or not role or role is None: return False
        if get_role(role) is None or get_permission(perm) is None: return False
        trans = sqlhub.processConnection.transaction()
        x = get_role(role)
        x.update_perm_relation(perm)
        trans.commit()
        return True
    except Exception as e:
        print(e)
        return False


def set_perm_relation( perm: list[tuple[str, str, str]] ):
    """

    Allow to set relations of a specific permission with an action and an object

    :param perm: A list of tuple containing [the perm name, the action name, the object name]
    :return: boolean

    """
    try:
        if not perm or perm is None: return False
        for x in perm:
            if len(x) != 3: return False
            if get_action(x[1]) is None or get_obj(x[2]) is None: return False
            trans = sqlhub.processConnection.transaction()
            x = perm_relations(x[0], x[1], x[2])
            if not x: return False
            trans.commit()
            return True
        return False
    except Exception as e:
        print(e)
        return False


def create_complete_schema( schema: str, action_display_name: list[str], metaattr: list[tuple[str, str]],
                            obj: list[str],
                            attr: list[tuple[str, str, str]] ):
    """

    Allow to create a complete schema and related object, actions, meta attributes, attrobites

    :param schema: The name of the schema
    :param action_display_name: list of name for the action
    :param metaattr: a list of tuple containing the meta attribute display_name and the type
    :param obj: a list of object containing their name
    :param attr: a list of tuple containing the name and the related object and meta attribute
    :return: boolean

    """
    try:
        var_bool = list()
        var_bool.append(create_schema(schema, action_display_name, metaattr))
        for x in obj: var_bool.append(create_object(x, schema))
        for x in attr: var_bool.append(create_attribute(x[0], x[1], x[2]))
        return all(var_bool)
    except Exception as e:
        print(e)
        return False


def create_complete_role( role: list[str], user: list[tuple[str, str]] ):
    """

    Allow to create a complete perm and related user

    :param role: a list of perm name
    :param user: a list of tuple containing a string of the user and the related perm to link to
    :return:

    """
    try:
        for x in role: add_role(x)
        for x in user: add_user(x[0], x[1])
        return True
    except Exception as e:
        print(e)
        return False
