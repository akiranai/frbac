from api.model_table_creator import create_complete_role, set_user_role, create_role, create_user, create_object, \
    create_metaattr, create_attribute, create_permission, set_perm_relation, set_perm_role
from api.model_simple import remove_role as del_role, remove_user as del_user, get_schema, \
    remove_permission as del_permission, get_role, get_obj, get_action, set_user_key_salt, get_user_salt, get_user_key
from api.tools import salt_gen, sbkdf2_super
from model import Frbacrole, Frbacuser, Frbacobject, Frbacaction, Frbacpermission, Frbacschema


def add_role(role_name: str) -> int:
    """

    Allow to create a perm

    :param role_name: The name of the role
    :return the role id

    """
    create_role(role_name)
    return Frbacrole.selectBy(name=role_name)[0].id


def remove_role(role_name: str):
    """

    Remove a role from the DB

    :param role_name: the name of the role to remove

    """
    del_role(role_name)


def add_user(username: str) -> int:
    """

    Allow to create an user

    :param username: the name of the user

    """
    create_user(username)
    return Frbacuser.selectBy(username=username)[0].id


def remove_user(username: str):
    """

    Remove a user from the db

    :param username: The name of the user

    """
    del_user(username)


def set_user_password(username: str, password: str):
    """

    Set to a user a password

    :param password:
    :param username:

    """
    salt = salt_gen()
    key = sbkdf2_super(pwd=password, salt=salt)
    set_user_key_salt(username=username, key=key, salt=salt)


def check_user_password(username: str, password: str):
    """

    check if the password is correct

    :param username:
    :param password:
    :return: Boolean

    """
    return sbkdf2_super(pwd=password, salt=get_user_salt(username)) == get_user_key(username)


def list_users_of_role(role_name: str) -> list:
    """

    Return a list of all related users of a role

    :param role_name: the name of the role we want to list users

    """
    return list(Frbacrole.selectBy(name=role_name)[0].LinkedUsers)


def list_roles_of_user(username: str) -> list:
    """

    Return a list of all related roles of a user

    :param username: the name of the username we want to list roles

    """
    return list(Frbacuser.selectBy(username=username)[0].LinkedRole)


def set_user_to_role(username: str, role_name: str):
    """

    Allow to define the related perm of an user

    :param role_name: The name of the user
    :param username: The name of the perm
    :return: boolean

    """
    x = set_user_role(username, role_name)
    if x: return True
    return False


def add_object(schema_name: str, metaattr: list[tuple[str, str]], attr: list[tuple[str, str, str]], obj_name: str) -> int:
    """

    Allow to add an object with meta attribute and attribute. The schema must exist

    """
    if get_schema(schema_name) is None: return None
    create_object(schema=schema_name,obj=obj_name)
    for x in metaattr: create_metaattr(x[0], x[1], schema_name)
    for x in attr: create_attribute(x[0], x[1], x[2])
    return Frbacobject.selectBy(name=obj_name)[0].id


def add_permission(perm_name: str, obj_name: str, action_name:str) -> int:
    """

    Allow to add permission. The obj and action must exist

    :param perm_name: the name of the permission
    :param obj_name: The name of the object
    :param action_name: the name of the action

    """
    if get_obj(obj_name) is None or get_action(action_name) is None: return None
    create_permission(perm_name)
    related_schema = Frbacaction.selectBy(action=action_name)[0].frbacschema
    set_perm_relation([(perm_name, action_name, obj_name)])
    return Frbacpermission.selectBy(name=perm_name)[0].id


def remove_permission(perm_name: str):
    """

    Remove a permission from  the db

    :param perm_name: The name of the permission you want to remove

    """
    del_permission(perm_name)


def list_all_schema() -> list:
    """

    Return a list of all schema existing in the DB

    """
    return list(Frbacschema.select())


def list_actions_of_schema(schema_name: str) -> list:
    """

    Return a list of all related actions of a schema

    :param schema_name: The name of the schema

    """
    return list(get_schema(schema_name).LinkedActions)


def list_objects_of_schema(schema_name: str) -> list:
    """

    Return a list of all related objects of a schema

    :param schema_name:

    """
    return list(get_schema(schema_name).LinkedObjects)


def list_permissions_of_schema(schema_name: str) -> list:
    """

    Return a list of all permissions existing related to a schema

    :param schema_name:

    """
    x = list()
    for elem in get_schema(schema_name).LinkedActions:
        print(elem.LinkedPerm)
        x += elem.LinkedPerm
    return x


def set_perm_to_role(perm_name: str, role_name: str):
    """

    Allow to set  a permission to a role

    :param perm_name:
    :param role_name:

    """
    set_perm_role(perm_name, role_name)


def remove_perm_from_role(perm_name: str, role_name: str):
    """

    Allow to remove a permission from a role

    :param perm_name:
    :param role_name:

    """
    get_role(role_name).remove_perm_relation(perm_name)
