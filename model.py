#!/usr/bin/env python3

from sqlobject import *

# TEMP !  Only for in memory test usage, please refear to SQLObject to connect to a production DB
sqlhub.processConnection = connectionForURI('sqlite:/:memory:')


class Frbacaction(SQLObject):
    action = StringCol(alternateID=True)
    display_name = StringCol()
    # 1-N
    LinkedPerm = MultipleJoin('Frbacpermission')
    frbacschema = ForeignKey('Frbacschema')

    def remove_schema_relation(self, schema):
        if self.frbacschema == schema: self.frbacschema = None

    def update_schema_relation(self, schema):
        self.frbacschema = schema


class Frbacschema(SQLObject):
    name = StringCol(alternateID=True)
    LinkedObjects = MultipleJoin('Frbacobject')
    LinkedActions = MultipleJoin('Frbacaction')
    LinkedMetaAttr = MultipleJoin('Frbacmetaattribute')


class Frbacobject(SQLObject):
    name = StringCol(alternateID=True)
    frbacschema = ForeignKey('Frbacschema', notNull=True, cascade=True)
    # 1-N
    LinkedAttributes = MultipleJoin('Frbacattribute')
    LinkedPerm = MultipleJoin('Frbacpermission')

    def remove_schema_relation(self, schema):
        if self.frbacschema == schema: self.frbacschema = None

    def update_schema_relation(self, schema):
        self.frbacschema = schema


class Frbacattribute(SQLObject):
    value = StringCol(alternateID=True)
    frbacobject = ForeignKey('Frbacobject')
    frbacmetaattribute = ForeignKey('Frbacmetaattribute')

    def remove_obj_relation(self, obj):
        if obj == self.frbacobject: self.frbacobject = None

    def update_obj_relation(self, obj):
        self.frbacobject = obj

    def remove_meta_attr_relation(self, meta_attr):
        if meta_attr == self.frbacmetaattribute: self.frbacmetaattribute = None

    def update_meta_attr_relation(self, meta_attr):
        self.frbacmetaattribute = meta_attr


class Frbacmetaattribute(SQLObject):
    name = StringCol(alternateID=True)
    display_name = StringCol()
    type_str = StringCol()
    frbacschema = ForeignKey('Frbacschema')
    LinkedAttributes = MultipleJoin('Frbacattribute')

    def remove_schema_relation(self, schema):
        if schema == self.frbacschema: self.frbacschema = None

    def update_schema_relation(self, schema):
        self.frbacschema = schema


class Frbacuser(SQLObject):
    username = StringCol(alternateID=True)
    LinkedRole = RelatedJoin('Frbacrole')
    salt = BLOBCol(default=None)
    key = BLOBCol(default=None)


class Frbacrole(SQLObject):
    name = StringCol(alternateID=True)
    LinkedUsers = RelatedJoin('Frbacuser')
    LinkedPerm = RelatedJoin('Frbacpermission')

    def update_user_relation(self, user: str):
        """
            Add a new relation between ROLE & USER
        """
        try:
            # if except directly that mean LinkedUser is empty
            # Check if the user key exist but has no more link because has been removed
            bool_link = any(elem for elem in self.LinkedUsers if elem.username == user)
            if not bool_link and Frbacuser.byUsername(user):
                the_user = Frbacuser.byUsername(user)
                # the_user is not empty and the name exist thus we should not get except
                self.addFrbacuser(the_user)

        except Exception as e:
            print(e)
            # LinkedUser is empty
            new_user = Frbacuser(username=user)
            self.addFrbacuser(new_user)

    def remove_user_relation(self, user: str):
        """
            Remove a relation between a user and perm
        """
        try: self.removeFrbacuser(Frbacuser.byUsername(user))
        except Exception as e:
            print(e)
            pass

    def update_perm_relation(self, perm: str):
        """
            Add a new relation between ROLE & PERM
        """
        try:
            # if except directly that mean LinkedPerm is empty
            # Check if the user key exist but has no more link because has been removed
            bool_link = any(elem for elem in self.LinkedPerm if elem.name == perm)
            if not bool_link and Frbacpermission.byName(perm):
                self.addFrbacpermission(Frbacpermission.byName(perm))
        except Exception as e:
            print(e)
            self.addFrbacuser(Frbacpermission(name=perm))

    def remove_perm_relation(self, perm: str):
        """
            Remove a relation between a user and perm
        """
        try: self.removeFrbacpermission(Frbacpermission.byName(perm))
        except Exception as e:
            print(e)
            pass


class Frbacpermission(SQLObject):
    name = StringCol(alternateID=True)
    frbacaction = ForeignKey('Frbacaction')
    frbacobject = ForeignKey('Frbacobject')
    LinkedRole = RelatedJoin('Frbacrole')


