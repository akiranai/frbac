import pytest

import api.model_simple as MS
import api.model_tools as MT
import api.string_constants as CONST
from api.tools import random_string


def test_add_related_join_role_user(db_init):
    """
        Try to link a user to a perm
    """
    role = MS.add_role('Hero')
    role.update_user_relation('Batman')
    role.update_user_relation('Superman')
    assert any(elem for elem in role.LinkedUsers if elem.username == 'Batman')


def test_remove_related_join_role_user(db_init):
    """
        Try to link a user to a perm and then remove the relation
    """
    # Add
    role = MS.add_role('hero')
    role.update_user_relation('Batman')
    assert MT.any_linked(role.LinkedUsers, 'username', 'Batman'), CONST.VALUE_RELATED_NOT_FOUND.format('Batman')
    # Remove the relation
    role.remove_user_relation('Batman')
    assert not MT.any_linked(role.LinkedUsers, 'username', 'Batman'), CONST.VALUE_RELATED_FOUND.format('Batman')


def test_related_join_role_user_duplicate(db_init):
    """
        Check that we cannot duplicate a user in a perm
    """
    role_name = random_string(5)
    user_name = random_string(5)
    role = MS.add_role(role_name)
    role.update_user_relation(user_name)
    assert MT.any_linked(role.LinkedUsers, 'username', user_name)
    # Try to read it
    with pytest.raises(Exception):
        assert role.update_user_relation(user_name)
