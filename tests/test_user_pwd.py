import os

from api.model_simple import get_user, set_user_key_salt, get_user_key, get_user_salt
from api.tools import random_string, sbkdf2_super, salt_gen
from api_app import add_user
from random import randint


def test_password(db_init):
    tmp_user = random_string(10)
    pwd = random_string(16)
    salt = salt_gen()
    key = sbkdf2_super(pwd=pwd, salt=salt)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    the_user = get_user(tmp_user)
    assert set_user_key_salt(username=tmp_user, key=key, salt=salt)
    the_user_key = get_user_key(tmp_user)
    # https://nitratine.net/blog/post/how-to-hash-passwords-in-python/
    # Wrong password
    new_key = sbkdf2_super(pwd=random_string(randint(5, 10)), salt=get_user_salt(tmp_user))
    assert the_user_key != new_key, "Keys are not the same !"

    # Correct password
    new_key = sbkdf2_super(pwd=pwd, salt=get_user_salt(tmp_user))
    assert the_user_key == new_key, "The key are not the same but they should"
