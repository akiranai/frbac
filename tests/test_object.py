from api.model_table_creator import create_object, create_schema
from api.model_simple import get_obj, remove_object
from api.tools import *
import model
import pytest


def test_create_object(db_init):
    """
        Try to create schema with specific methods that return bool
        mt.create_object("obj_name", "schema")
    """
    tmp_schema = random_string(10)
    assert create_schema(tmp_schema)
    assert create_object(random_string(10), tmp_schema)
    assert create_object(random_string(10), random_string(10)) is False
    assert create_object(None, None) is False
    assert create_object(random_string(10), None) is False
    assert create_object(None, tmp_schema) is False
    assert create_object("", tmp_schema) is False
    assert create_object("", "") is False


def test_get_action(db_init):
    """
        Try to get a specific object
    """
    tmp_schema = random_string(10)
    tmp_object = random_string(10)
    assert create_schema(tmp_schema)
    assert create_object(tmp_object, tmp_schema)
    assert get_obj(tmp_object)
    assert get_obj(random_string(10)) is None


def test_remove_object(db_init):
    """
        Remove a specific object
    """
    tmp_schema = random_string(10)
    tmp_object = random_string(10)
    assert create_schema(tmp_schema)
    assert create_object(tmp_object, tmp_schema)
    assert get_obj(tmp_object)
    remove_object(tmp_object)
    with pytest.raises(Exception):
        assert model.Frbacobject.byName(tmp_object)

