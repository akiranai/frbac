import pytest
from api.tools import random_string
from api.model_simple import get_user, add_user, remove_user


def test_create_user(db_init):
    """
        Try to create a perm
    """
    tmp_user = random_string(10)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    assert add_user(tmp_user) is None


def test_get_role(db_init):
    """
        Try to get a perm
    """
    tmp_user = random_string(10)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    assert get_user(random_string(5)) is None


def test_remove_user(db_init):
    """
        Check if we remove the perm, it is actually removed in the DB
    """
    tmp_user = random_string(10)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    remove_user(tmp_user)
    with pytest.raises(Exception):
        assert get_user(tmp_user)
