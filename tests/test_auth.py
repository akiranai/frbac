from api import auth
from api import model_table_creator as MTC
from api import tools
from api.model_simple import set_user_key_salt, set_user_pwd
from api.tools import random_string
from api.model_table_creator import create_complete_schema, create_complete_role, create_permission, set_perm_relation, \
    set_perm_role


def test_auth(db_init):
    schema = random_string(10)
    action_display_name = [random_string(10)]
    metaattr = [(random_string(10), random_string(10)), (random_string(10), random_string(10))]
    obj = [random_string(10), random_string(10)]
    attr = [(random_string(10), obj[0], schema + "_" + metaattr[0][0])]
    perm = random_string(5)
    assert create_permission(perm)
    assert create_complete_schema(schema, action_display_name, metaattr, obj, attr)
    role = [random_string(5)]
    user = [random_string(5)]
    assert create_complete_role(role, [(user[0], role[0])])
    tmp_action_name = schema + "_" + action_display_name[0]
    perm_rel = [(perm, tmp_action_name, obj[0])]
    assert set_perm_relation(perm_rel)
    assert set_perm_role(perm, role[0])
    assert auth.request_auth_join(usr=user[0], display_name=action_display_name[0], obj=obj[0])
    assert not auth.request_auth_join(usr=tools.random_string(10), display_name=action_display_name[0], obj=obj[0])


def test_auth_pwd(db_init):
    schema = random_string(10)
    action_display_name = [random_string(10)]
    metaattr = [(random_string(10), random_string(10)), (random_string(10), random_string(10))]
    obj = [random_string(10), random_string(10)]
    attr = [(random_string(10), obj[0], schema + "_" + metaattr[0][0])]
    perm = random_string(5)
    assert create_permission(perm)
    assert create_complete_schema(schema, action_display_name, metaattr, obj, attr)
    role = [random_string(5)]
    user = [random_string(5)]
    pwd = random_string(10)
    assert create_complete_role(role, [(user[0], role[0])])
    tmp_action_name = schema + "_" + action_display_name[0]
    perm_rel = [(perm, tmp_action_name, obj[0])]
    assert set_perm_relation(perm_rel)
    assert set_perm_role(perm, role[0])
    set_user_pwd(username=user[0], pwd=pwd)
    assert auth.request_auth_join(usr=user[0], display_name=action_display_name[0], obj=obj[0], pwd=pwd)
    assert not auth.request_auth_join(usr=user[0], display_name=action_display_name[0], obj=obj[0]
                                      , pwd=random_string(10))
    assert not auth.request_auth_join(usr=tools.random_string(10), display_name=action_display_name[0]
                                      , obj=obj[0], pwd=random_string(10))
