import pytest

from api.model_simple import get_action, remove_action
from api.model_table_creator import create_action, create_schema
import api.string_constants as CONST
import model
from api.tools import random_string


def test_create_action(db_init):
    """
        Try to create schema with specific methods that return bool
        mt.create_action(display_name, schema])
    """
    var_test_1 = random_string(10)
    assert create_schema(var_test_1)
    assert create_action(random_string(10), var_test_1)
    assert create_action(random_string(10), None) is False
    assert create_action(None, var_test_1) is False
    tmp_display_name = random_string(5)
    var_test_2 = random_string(10)
    assert create_schema(var_test_2)
    assert create_action(tmp_display_name, var_test_2)
    assert create_action(tmp_display_name, var_test_2) is False


def test_get_action(db_init):
    """
        Try to get a specific action
    """
    tmp_display_name = random_string(5)
    tmp_schema = random_string(5)
    var_a = tmp_schema + "_" + tmp_display_name
    assert create_schema(tmp_schema)
    assert create_action(tmp_display_name, tmp_schema) is True
    assert get_action(var_a), CONST.VALUE_NOT_FOUND.format(var_a, 'ACTION')


def test_remove_action(db_init):
    """
        Try to remove an action
    """
    tmp_display_name = random_string(5)
    tmp_schema = random_string(5)
    var_a = tmp_schema + "_" + tmp_display_name
    assert create_schema(tmp_schema)
    assert create_action(tmp_display_name, tmp_schema) is True
    assert get_action(var_a), CONST.VALUE_NOT_FOUND.format(var_a, 'ACTION')
    remove_action(var_a)
    with pytest.raises(Exception):
        assert model.Frbacaction.byAction(var_a)
