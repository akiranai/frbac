import pytest

import model
from api.model_simple import get_metaattr, remove_metaattr
from api.model_table_creator import create_schema, create_metaattr
import api.string_constants as CONST
from api.tools import *


def test_create_metaattr(db_init):
    """
        Try to create metaattr with specific methods that return bool
        mt.create_metaattr("display_name", "type", "schema")
    """
    var_test_1 = random_string(10)
    assert create_schema(var_test_1)
    assert create_metaattr(random_string(10), random_string(3), var_test_1)
    assert create_metaattr(random_string(10), None, var_test_1) is False
    assert create_metaattr(None, random_string(10), var_test_1) is False
    assert create_metaattr("", random_string(10), var_test_1) is False
    assert create_metaattr(random_string(10), "", var_test_1) is False
    assert create_metaattr(random_string(10), random_string(10), None) is False
    assert create_metaattr(random_string(10), random_string(10), random_string(10)) is False

    var_test_2 = random_string(10)
    tmp_display_name = random_string(10)
    tmp_type = random_string(3)
    assert create_schema(var_test_2)
    assert create_metaattr(tmp_display_name, tmp_type, var_test_2)
    assert create_metaattr(tmp_display_name, tmp_type, var_test_2) is False


def test_get_mettattr(db_init):
    """
        Try to get a specific meta attribute
    """
    tmp_schema = random_string(10)
    tmp_display_name = random_string(10)
    tmp_type = random_string(3)
    var_a = tmp_schema + "_" + tmp_display_name
    assert create_schema(tmp_schema)
    assert create_metaattr(tmp_display_name, tmp_type, tmp_schema)
    assert get_metaattr(var_a), CONST.VALUE_NOT_FOUND.format(var_a, 'METAATTR')


def test_remove_mettaattr(db_init):
    """
        Try to remove a specific meta attribute
    """
    tmp_display_name = random_string(5)
    tmp_type = random_string(3)
    tmp_schema = random_string(5)
    var_a = tmp_schema + "_" + tmp_display_name
    assert create_schema(tmp_schema)
    assert create_metaattr(tmp_display_name, tmp_type, tmp_schema)
    assert get_metaattr(var_a), CONST.VALUE_NOT_FOUND.format(var_a, 'METAATTR')
    remove_metaattr(var_a)
    with pytest.raises(Exception):
        assert model.Frbacmetaattribute.byName(var_a)



