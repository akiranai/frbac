import inspect

import api.string_constants as CONST
import model


def test_table(db_init):
    """
        Check if all table are well created
    """
    for cls in inspect.getmembers(model, inspect.isclass):
        if cls[1].__module__ == 'model':
            assert cls[1].select() is not None, CONST.TABLE_NOT_EXIST.format(cls[0])

