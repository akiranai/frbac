import api.model_data_show as MDS
from api.tools import random_string
from api.model_table_creator import create_complete_schema, create_complete_role, set_perm_relation, set_perm_role, create_permission


def test_create_complete_schema(db_init):
    schema = random_string(10)
    action_display_name = [random_string(10)]
    metaattr = [(random_string(10), random_string(10))]
    obj = [random_string(10), random_string(10)]
    attr = [(random_string(10), obj[0], schema + "_" + metaattr[0][0])]
    assert create_complete_schema(schema, action_display_name, metaattr, obj, attr)


def test_create_complete_role(db_init):
    role = [random_string(5)]
    user = [random_string(5)]
    assert create_complete_role(role, user)


def test_create_complete_schema_role(db_init):
    schema = random_string(10)
    action_display_name = [random_string(10)]
    metaattr = [(random_string(10), random_string(10)), (random_string(10), random_string(10))]
    obj = [random_string(10), random_string(10)]
    attr = [(random_string(10), obj[0], schema + "_" + metaattr[0][0])]
    assert create_complete_schema(schema, action_display_name, metaattr, obj, attr)
    role = [random_string(5)]
    user = [random_string(5)]
    perm = random_string(5)
    assert create_complete_role(role, [(user[0], role[0])])
    tmp_action_name = schema + "_" + action_display_name[0]
    perm_rel = [(perm, tmp_action_name, obj[0])]
    assert create_permission(perm)
    assert set_perm_relation(perm_rel)
    assert set_perm_role(perm, role[0])
    MDS.print_every_infos(schema)
