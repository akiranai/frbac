import pytest

import api.model_simple as MS
import api.string_constants as CONST
from api.tools import random_string

from api.model_table_creator import create_schema, create_object, create_metaattr, create_attribute
from model import Frbacattribute


def test_add_attr(db_init):
    """
        Try to add a new attribute
    """
    var_schema = random_string(10)

    var_meta_display_name = random_string(10)
    var_meta_type = random_string(3)
    var_meta_name = var_schema + "_" + var_meta_display_name
    var_object = random_string(10)

    assert create_schema(var_schema)
    assert create_object(var_object, var_schema)
    assert create_metaattr(var_meta_display_name, var_meta_type, var_schema)

    assert create_attribute(random_string(5), var_object, var_meta_name)
    assert create_attribute(random_string(5), None, var_meta_name) is False
    assert create_attribute(None, None, None) is False
    assert create_attribute(random_string(5), random_string(10), None) is False
    assert create_attribute(random_string(5), var_object, random_string(10)) is False

    var_attr = random_string(5)
    assert create_attribute(var_attr, var_object, var_meta_name)
    assert create_attribute(var_attr, var_object, var_meta_name) is False


def test_get_attr(db_init):
    """
        Try to get a specific attribute
    """
    var_schema = random_string(10)

    var_meta_display_name = random_string(10)
    var_meta_type = random_string(3)
    var_meta_name = var_schema + "_" + var_meta_display_name
    var_object = random_string(10)
    var_attr = random_string(10)
    assert create_schema(var_schema)
    assert create_object(var_object, var_schema)
    assert create_metaattr(var_meta_display_name, var_meta_type, var_schema)

    assert create_attribute(var_attr, var_object, var_meta_name)
    assert MS.get_attr(var_attr), CONST.VALUE_NOT_FOUND.format(var_attr, 'ATTRIBUTE')


def test_remove_attr(db_init):
    """
        Try to remove a specific attribute
    """
    var_schema = random_string(10)

    var_meta_display_name = random_string(10)
    var_meta_type = random_string(3)
    var_meta_name = var_schema + "_" + var_meta_display_name
    var_object = random_string(10)
    var_attr = random_string(10)
    assert create_schema(var_schema)
    assert create_object(var_object, var_schema)
    assert create_metaattr(var_meta_display_name, var_meta_type, var_schema)

    assert create_attribute(var_attr, var_object, var_meta_name)
    assert MS.get_attr(var_attr), CONST.VALUE_NOT_FOUND.format(var_attr, 'ATTRIBUTE')
    MS.remove_attr(var_attr)
    with pytest.raises(Exception):
        assert Frbacattribute.byName(var_attr)
