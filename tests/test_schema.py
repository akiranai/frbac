import pytest

import api.model_simple as MS
import api.model_table_creator as MTC
import api.string_constants as CONST
from api.tools import random_string


def test_create_schema(db_init):
    """
        Try to create a schema
        The signature of the schema is the following:
        create_schema(name:str, [actions:str], [(metaattr_name:str, metaattr_type:str)])
    """
    assert MTC.create_schema(random_string(10), [random_string(5)]) is True
    assert MTC.create_schema(random_string(10), []) is False
    assert MTC.create_schema(random_string(10)) is True
    tmp = random_string(5)
    assert MTC.create_schema(random_string(10), [tmp, tmp]) is False
    assert MTC.create_schema(random_string(10), [random_string(5), random_string(5)]) is True
    assert MTC.create_schema(random_string(10), [random_string(5)], [(random_string(5), random_string(5))]) is True
    assert MTC.create_schema(random_string(10), [random_string(5)], [(random_string(5))]) is False
    assert MTC.create_schema(random_string(10), [random_string(5)], None) is True
    assert MTC.create_schema(random_string(10), [random_string(5)], [None]) is False
    tmp = random_string(5)
    assert MTC.create_schema(random_string(10), [random_string(5)], [tmp, tmp]) is False
    assert MTC.create_schema(random_string(10), [random_string(5)], []) is True
    assert MTC.create_schema(random_string(10), [random_string(5)], [(random_string(5), random_string(5)),
                                                                     (random_string(5), random_string(5))]) is True


def test_get_schema(db_init):
    """
        Try to get a specific schema
    """
    tmp_schema = random_string(10)
    assert MTC.create_schema(tmp_schema)
    assert MS.get_schema(tmp_schema)


def test_remove_schema(db_init):
    """
        Try to remove a specific schema
    """
    tmp_schema = random_string(10)
    assert MTC.create_schema(tmp_schema)
    assert MS.get_schema(tmp_schema)
    MS.remove_schema(tmp_schema)
    with pytest.raises(Exception):
        assert MS.get_schema(tmp_schema)
