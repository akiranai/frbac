import pytest

from api.model_simple import get_user, get_permission
from api.model_table_creator import create_schema, create_action
from api.tools import random_string
from api_app import *


def test_add_role(db_init):
    tmp_role = random_string(5)
    assert add_role(tmp_role)
    assert get_role(tmp_role)
    assert get_role(random_string(5)) is None


def test_remove_role(db_init):
    tmp_role = random_string(5)
    assert add_role(tmp_role)
    assert get_role(tmp_role)
    remove_role(tmp_role)
    assert get_role(tmp_role) is None


def test_add_user(db_init):
    tmp_user = random_string(5)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    assert get_user(random_string(5)) is None


def test_remove_user(db_init):
    tmp_user = random_string(5)
    assert add_user(tmp_user)
    assert get_user(tmp_user)
    assert get_user(random_string(5)) is None


def test_list_users_of_role(db_init):
    tmp_user = [random_string(5) for x in range(5)]
    tmp_role = random_string(5)

    assert add_role(tmp_role)
    assert get_role(tmp_role)
    for x in tmp_user:
        assert add_user(x)
        assert get_user(x)
        assert set_user_to_role(x, tmp_role)
    tmp_list = list_users_of_role(tmp_role)
    assert len(tmp_list) == 5
    for x in tmp_list: assert x.username in tmp_user


def test_list_roles_of_user(db_init):
    tmp_user = random_string(5)
    tmp_role = [random_string(5) for x in range(5)]

    assert add_user(tmp_user)
    assert get_user(tmp_user)
    for x in tmp_role:
        assert add_role(x)
        assert get_role(x)
        assert set_user_to_role(tmp_user, x)
    tmp_list = list_roles_of_user(tmp_user)
    assert len(tmp_list) == 5
    for x in tmp_list: assert x.name in tmp_role


def test_add_object(db_init):
    schema = random_string(5)
    obj = random_string(5)
    create_schema(schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    assert get_obj(random_string(5)) is None


def test_add_permission(db_init):
    schema = random_string(5)
    obj = random_string(5)
    perm = random_string(5)
    act = random_string(5)
    create_schema(schema)
    create_action(act, schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    assert add_permission(perm, obj, schema + "_" + act)
    assert get_permission(perm)
    assert get_permission(random_string(5)) is None


def test_remove_permission(db_init):
    schema = random_string(5)
    obj = random_string(5)
    perm = random_string(5)
    act = random_string(5)
    create_schema(schema)
    create_action(act, schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    assert add_permission(perm, obj, schema + "_" + act)
    assert get_permission(perm)
    remove_permission(perm)
    assert get_permission(perm) is None


def test_list_all_schema(db_init):
    list_schema = [random_string(5) for x in range(5)]
    for x in list_schema: create_schema(x)
    tmp_list = list_all_schema()
    assert len(tmp_list) == 5
    for x in tmp_list: assert x.name in list_schema


def test_list_actions_of_schema(db_init):
    schema = random_string(5)
    act = [random_string(5) for x in range(5)]
    create_schema(schema)
    for x in act:
        assert create_action(x, schema)
        assert get_action(schema + "_" + x)
    assert get_action(random_string(5)) is None
    x = list_actions_of_schema(schema)
    assert len(x) == 5
    for y in get_schema(schema).LinkedActions: assert y.display_name in act


def test_list_objects_of_schema(db_init):
    schema = random_string(5)
    obj = [random_string(5) for x in range(5)]
    create_schema(schema)
    for x in obj:
        assert create_object(x, schema)
        assert get_obj(x)
    assert get_obj(random_string(5)) is None
    x = list_objects_of_schema(schema)
    assert len(x) == 5
    for y in get_schema(schema).LinkedObjects: assert y.name in obj


def test_list_permissions_of_schema(db_init):
    schema = random_string(5)
    obj = random_string(5)
    perm = [random_string(5) for x in range(5)]
    act = random_string(5)
    create_schema(schema)
    create_action(act, schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    for x in perm:
        assert add_permission(x, obj, schema + "_" + act)
        assert get_permission(x)
    x = list_permissions_of_schema(schema)
    assert len(x) == 5


def test_set_perm_to_role(db_init):
    schema = random_string(5)
    obj = random_string(5)
    perm = random_string(5)
    act = random_string(5)
    create_schema(schema)
    create_action(act, schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    assert add_permission(perm, obj, schema + "_" + act)
    assert get_permission(perm)
    role = random_string(5)
    assert add_role(role)
    set_perm_to_role(perm, role)
    assert perm in get_role(role).LinkedPerm[0].name


def test_remove_perm_from_role(db_init):
    schema = random_string(5)
    obj = random_string(5)
    perm = random_string(5)
    act = random_string(5)
    create_schema(schema)
    create_action(act, schema)
    meta_attr_display = random_string(5)
    tuple_metaattr = (meta_attr_display, random_string(2))
    tuple_attr = (random_string(5), obj, meta_attr_display)
    assert add_object(schema, [tuple_metaattr], [tuple_attr], obj) is not None
    assert get_obj(obj)
    assert add_permission(perm, obj, schema + "_" + act)
    assert get_permission(perm)
    role = random_string(5)
    assert add_role(role)
    set_perm_to_role(perm, role)
    assert perm in get_role(role).LinkedPerm[0].name
    remove_perm_from_role(perm, role)
    with pytest.raises(Exception):
        assert perm in get_role(role).LinkedPerm[0].name