import pytest
from api.model_simple import add_role, get_role, remove_role
from api.tools import random_string


def test_create_role(db_init):
    """
        Try to create a perm
    """
    tmp_role = random_string(10)
    assert add_role(tmp_role)
    assert get_role(tmp_role)
    assert add_role(tmp_role) is None


def test_get_role(db_init):
    """
        Try to get a perm
    """
    tmp_role = random_string(10)
    assert add_role(tmp_role)
    assert get_role(tmp_role)
    assert get_role(random_string(5)) is None


def test_remove_role(db_init):
    """
        Check if we remove the perm, it is actually removed in the DB
    """
    tmp_role = random_string(10)
    assert add_role(tmp_role)
    assert get_role(tmp_role)
    remove_role(tmp_role)
    with pytest.raises(Exception):
        assert get_role(tmp_role)
