from api.tools import random_string
from api.model_table_creator import create_action, create_object, create_schema, create_role, set_perm_relation, \
    set_perm_role
from api.model_simple import add_permission, get_permission, remove_permission
import pytest
from api.model_tools import any_linked


def test_create_permissions(db_init):
    """
        Try to create a simple permission
    """
    tmp_perm = random_string(10)
    assert add_permission(tmp_perm)
    assert get_permission(tmp_perm)
    assert add_permission(tmp_perm) is None


def test_get_role(db_init):
    """
        Try to get permµ
    """
    tmp_perm = random_string(10)
    assert add_permission(tmp_perm)
    assert get_permission(tmp_perm)
    assert get_permission(random_string(10)) is None


def test_remove_role(db_init):
    """
        Check if we remove the permission its is actually removed from the DB
    """
    tmp_perm = random_string(10)
    assert add_permission(tmp_perm)
    assert get_permission(tmp_perm)
    remove_permission(tmp_perm)
    with pytest.raises(Exception):
        assert get_permission(tmp_perm)


def test_complete_permissions(db_init):
    """
        Try to create a complete permissions with his FK and perm MtM relationship
    """
    tmp_act = random_string(10)
    tmp_schem = random_string(10)
    tmp_object = random_string(10)
    assert create_schema(tmp_schem)
    assert create_action(tmp_act, tmp_schem)
    assert create_object(tmp_object, tmp_schem)
    tmp_role = random_string(10)
    assert create_role(tmp_role)

    tmp_perm = random_string(10)
    assert add_permission(tmp_perm)
    assert set_perm_relation([(tmp_perm, tmp_schem + '_' + tmp_act, tmp_object)])
    assert set_perm_role(tmp_perm, tmp_role)

    x = get_permission(tmp_perm)
    assert tmp_object == x.frbacobject.name
    assert tmp_act == x.frbacaction.display_name
    assert random_string(10) != x.frbacobject.name
    assert random_string(10) != x.frbacaction.display_name
    assert x.LinkedRole
    assert any_linked(x.LinkedRole, 'name', tmp_role)


