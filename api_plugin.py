from api.model_simple import get_schema
from api.model_table_creator import create_complete_schema, create_schema, create_action
from model import Frbacschema, Frbacaction


def add_complete_schema(schema: str, action_display_name: list[str], metaattr: list[tuple[str, str]], obj: list[str],
               attr: list[tuple[str, str, str]]):
    """


    Allow to create a complete schema and related object, actions, meta attributes, attributes

    :param schema: The name of the schema
    :param action_display_name: list of name for the action
    :param metaattr: a list of tuple containing the meta attribute display_name and the type
    :param obj: a list of object containing their name
    :param attr: a list of tuple containing the name and the related object and meta attribute
    :return: boolean

    """
    create_complete_schema(schema, action_display_name, metaattr, obj, attr)


def add_schema(schema: str) -> int:
    """

    :param schema: The name of the schema
    :return: The ID of that schema

    """
    create_schema(schema)
    return Frbacschema.selectBy(name=schema)[0].id


def add_action_to_schema(schema: str, action: str):
    """

    :param schema: the name of the schema to link the new action
    :param action: the new action
    :return: the id of the action

    """
    if get_schema(schema) is None: return None
    create_action(action, schema)
    return Frbacaction.selectBy(action=action)[0].id


def list_all_schema() -> list:
    """

    Return a list of all schema existing in the DB

    """
    return list(Frbacschema.select())


def list_actions_of_schema(schema_name: str) -> list:
    """

    Return a list of all related actions of a schema
    :param schema_name: The name of the schema

    """
    return list(get_schema(schema_name).LinkedActions)