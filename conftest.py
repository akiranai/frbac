import pytest

from api.tools import random_string
from api.model_tools import table_create, table_drop


@pytest.fixture
def random_string_list():
    """
        Yield a list of random string
        [!] because of random package being use, do not use it for crypto [!]
    """
    import random
    yield [random_string(random.randrange(5, 15)) for x in range(5)]


@pytest.fixture
def db_init():
    """
        Create all table then destroy them
        ACTION | SCHEMA | OBJECT | ATTRIBUT
        METAATTRIBUTE | USER | ROLE
    """
    # Create all table
    table_create()
    yield
    # Destroy all table
    table_drop()



